package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/app")
public class StudentController {

    @Autowired
    StudentService studentService ;

    @PostMapping("/create")
    public Student addStudentToTable(@RequestBody Student student){
        return studentService.addStudent(student);
    }

    @GetMapping
    public String getInfo(){
        return "infooooo";
    }
}
